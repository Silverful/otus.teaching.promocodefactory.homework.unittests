﻿using AutoFixture;
using AutoFixture.AutoMoq;
using FluentAssertions;
using Microsoft.AspNetCore.Mvc;
using Moq;
using Otus.Teaching.PromoCodeFactory.Core.Abstractions.Repositories;
using Otus.Teaching.PromoCodeFactory.Core.Domain.PromoCodeManagement;
using Otus.Teaching.PromoCodeFactory.UnitTests.Builders;
using Otus.Teaching.PromoCodeFactory.WebHost.Controllers;
using Otus.Teaching.PromoCodeFactory.WebHost.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using Xunit;

namespace Otus.Teaching.PromoCodeFactory.UnitTests.WebHost.Controllers.Partners
{
    public class SetPartnerPromoCodeLimitAsyncTests
    {
        private readonly Mock<IRepository<Partner>> _partnersRepositoryMock;
        private readonly PartnersController _partnersController;

        public SetPartnerPromoCodeLimitAsyncTests()
        {
            var fixture = new Fixture().Customize(new AutoMoqCustomization());
            _partnersRepositoryMock = fixture.Freeze<Mock<IRepository<Partner>>>();
            
            _partnersController = fixture.Build<PartnersController>().OmitAutoProperties().Create();
        }

        public Partner CreateBasePartner()
        {
            var partner = new PartnerBuilder()
                .WithId(Guid.Parse("def47943-7aaf-44a1-ae21-05aa4948b165"))
                .WithName("Суперигрушки")
                .WithIsActive(true)
                .WithPartnerLimits(new List<PartnerPromoCodeLimit>() {new PartnerPromoCodeLimitBuilder()
                    .WithId(Guid.Parse("e00633a5-978a-420e-a7d6-3e1dab116393"))
                    .WithCreateDate(new DateTime(2020, 07, 9))
                    .WithEndDate(new DateTime(2020, 10, 9))
                    .WithLimit(100)
                    .Build() })
                .Build();

            return partner;
        }

        public SetPartnerPromoCodeLimitRequest CreateSetPartnerPromoCodeLimitRequest()
        {
            var setPartnerLimitRequest = new SetPartnerPromoCodeLimitRequestBuilder()
                .WithLimit(10)
                .WithDefaultEndDate()
                .Build();

            return setPartnerLimitRequest;
        }

        [Fact]
        public async void SetPartnerPromoCodeLimitAsync_PartnerNotFound_ReturnsNotFound()
        {
            var partnerId = Guid.Parse("def47943-7aaf-44a1-ae21-05aa4948b165");
            Partner partner = null;
            var setPartnerLimitRequest = CreateSetPartnerPromoCodeLimitRequest();

            _partnersRepositoryMock.Setup(repo => repo.GetByIdAsync(partnerId))
                .ReturnsAsync(partner);

            // Act
            var result = await _partnersController.SetPartnerPromoCodeLimitAsync(partnerId, setPartnerLimitRequest);

            // Assert
            result.Should().BeAssignableTo<NotFoundResult>();
        }

        [Fact]
        public async void SetPartnerPromoCodeLimitAsync_PartnerIsNotActive_ReturnsBadRequest()
        {
            // Arrange
            var partnerId = Guid.Parse("def47943-7aaf-44a1-ae21-05aa4948b165");
            var partner = CreateBasePartner();
            partner.IsActive = false;
            var setPartnerLimitRequest = CreateSetPartnerPromoCodeLimitRequest();

            _partnersRepositoryMock.Setup(repo => repo.GetByIdAsync(partnerId))
                .ReturnsAsync(partner);

            // Act
            var result = await _partnersController.SetPartnerPromoCodeLimitAsync(partnerId, setPartnerLimitRequest);

            // Assert
            result.Should().BeAssignableTo<BadRequestObjectResult>();
        }


        [Fact]
        public async void SetPartnerPromoCodeLimitAsync_SetNewLimitToPartner_ReturnsZero()
        {
            // Arrange
            var partnerId = Guid.Parse("def47943-7aaf-44a1-ae21-05aa4948b165");
            var partner = CreateBasePartner();
            var setPartnerLimitRequest = CreateSetPartnerPromoCodeLimitRequest();

            var partnerList = new List<Partner>();
            partnerList.Add(partner);
            _partnersRepositoryMock.Setup(repo => repo.GetByIdAsync(partnerId))
                .ReturnsAsync(partner);
            _partnersRepositoryMock.Setup(repo => repo.GetAllAsync())
                .ReturnsAsync(partnerList);

            // Act
            var result = await _partnersController.SetPartnerPromoCodeLimitAsync(partnerId, setPartnerLimitRequest);
            var partnerResult = await _partnersController.GetPartnersAsync();
            var partners = (partnerResult.Result as OkObjectResult).Value as IEnumerable<PartnerResponse>;
            var changedPartner = partners?.Where(p => p.Id == partnerId)?.FirstOrDefault();

            // Assert
            result.Should().BeAssignableTo<CreatedAtActionResult>();
            partnerResult.Result.Should().BeAssignableTo<OkObjectResult>();
            partners.Should().NotBeNull();
            changedPartner.Should().NotBeNull();
            changedPartner.NumberIssuedPromoCodes.Should().Be(0);
        }

        [Fact]
        public async void SetPartnerPromoCodeLimitAsync_OldLimitIsDisabled_ReturnsNull()
        {
            // Arrange
            var partnerId = Guid.Parse("def47943-7aaf-44a1-ae21-05aa4948b165");
            var partner = CreateBasePartner();
            var setPartnerLimitRequest = CreateSetPartnerPromoCodeLimitRequest();

            var partnerList = new List<Partner>();
            partnerList.Add(partner);
            _partnersRepositoryMock.Setup(repo => repo.GetByIdAsync(partnerId))
                .ReturnsAsync(partner);
            _partnersRepositoryMock.Setup(repo => repo.GetAllAsync())
                .ReturnsAsync(partnerList);

            // Act
            var result = await _partnersController.SetPartnerPromoCodeLimitAsync(partnerId, setPartnerLimitRequest);
            var partnerResult = await _partnersController.GetPartnersAsync();
            var partners = (partnerResult.Result as OkObjectResult).Value as IEnumerable<PartnerResponse>;
            var changedPartner = partners?.Where(p => p.Id == partnerId)?.FirstOrDefault();

            // Assert
            result.Should().BeAssignableTo<CreatedAtActionResult>();
            partnerResult.Result.Should().BeAssignableTo<OkObjectResult>();
            partners.Should().NotBeNull();
            changedPartner.Should().NotBeNull();
            changedPartner.PartnerLimits.Where(l => l.Limit == 100).FirstOrDefault().CancelDate.Should().NotBeNull();
        }

        [Fact]
        public async void SetPartnerPromoCodeLimitAsync_ZeroLimitNotAllowed_ReturnsBadRequest()
        {
            // Arrange
            var partnerId = Guid.Parse("def47943-7aaf-44a1-ae21-05aa4948b165");
            var partner = CreateBasePartner();
            var setPartnerLimitRequest = new SetPartnerPromoCodeLimitRequestBuilder()
                .WithLimit(0)
                .WithDefaultEndDate()
                .Build();

            var partnerList = new List<Partner>();
            partnerList.Add(partner);
            _partnersRepositoryMock.Setup(repo => repo.GetByIdAsync(partnerId))
                .ReturnsAsync(partner);
            _partnersRepositoryMock.Setup(repo => repo.GetAllAsync())
                .ReturnsAsync(partnerList);

            // Act
            var result = await _partnersController.SetPartnerPromoCodeLimitAsync(partnerId, setPartnerLimitRequest);

            // Assert
            result.Should().BeAssignableTo<BadRequestObjectResult>();
        }

        [Fact]
        public async void SetPartnerPromoCodeLimitAsync_NewLimitSavedToDB_ReturnsNewLimit()
        {
            // Arrange
            var partnerId = Guid.Parse("def47943-7aaf-44a1-ae21-05aa4948b165");
            var partner = CreateBasePartner();
            var setPartnerLimitRequest = CreateSetPartnerPromoCodeLimitRequest();

            var partnerList = new List<Partner>();
            partnerList.Add(partner);
            _partnersRepositoryMock.Setup(repo => repo.GetByIdAsync(partnerId))
                .ReturnsAsync(partner);
            _partnersRepositoryMock.Setup(repo => repo.GetAllAsync())
                .ReturnsAsync(partnerList);

            // Act
            var result = await _partnersController.SetPartnerPromoCodeLimitAsync(partnerId, setPartnerLimitRequest);
            var partnerResult = await _partnersController.GetPartnersAsync();
            var partners = (partnerResult.Result as OkObjectResult).Value as IEnumerable<PartnerResponse>;
            var changedPartner = partners?.Where(p => p.Id == partnerId)?.FirstOrDefault();

            // Assert
            result.Should().BeAssignableTo<CreatedAtActionResult>();
            partnerResult.Result.Should().BeAssignableTo<OkObjectResult>();
            partners.Should().NotBeNull();
            changedPartner.Should().NotBeNull();
            changedPartner.PartnerLimits.Where(l => l.Limit == 10).FirstOrDefault().Should().NotBeNull();
        }

        [Fact]
        public async void SetPartnerPromoCodeLimitAsync_NullRequestObject_ReturnsBadRequest()
        {
            // Arrange
            var partnerId = Guid.Parse("def47943-7aaf-44a1-ae21-05aa4948b165");

            // Act
            var result = await _partnersController.SetPartnerPromoCodeLimitAsync(partnerId, null);

            // Assert
            result.Should().BeAssignableTo<BadRequestObjectResult>();
        }
    }
}