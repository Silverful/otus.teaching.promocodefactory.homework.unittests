﻿using Otus.Teaching.PromoCodeFactory.WebHost.Models;
using System;

namespace Otus.Teaching.PromoCodeFactory.UnitTests.Builders
{
    public class SetPartnerPromoCodeLimitRequestBuilder
    {
        private int _limit;
        private DateTime _endDate;

        public SetPartnerPromoCodeLimitRequestBuilder WithLimit(int limit)
        {
            _limit = limit;
            return this;
        }

        public SetPartnerPromoCodeLimitRequestBuilder WithEndDate(DateTime endDate)
        {
            _endDate = endDate;
            return this;
        }

        public SetPartnerPromoCodeLimitRequestBuilder WithDefaultEndDate()
        {
            _endDate = DateTime.Now.AddDays(2);
            return this;
        }

        public SetPartnerPromoCodeLimitRequest Build()
        {
            return new SetPartnerPromoCodeLimitRequest() { Limit = _limit, EndDate = _endDate };
        }
    }
}
