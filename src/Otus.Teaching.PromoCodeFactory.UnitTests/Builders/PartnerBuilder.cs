﻿using Otus.Teaching.PromoCodeFactory.Core.Domain.PromoCodeManagement;
using System;
using System.Collections.Generic;

namespace Otus.Teaching.PromoCodeFactory.UnitTests.Builders
{
    public class PartnerBuilder
    {
        private Guid _id;
        private string _name;
        private bool _isActive;
        private ICollection<PartnerPromoCodeLimit> _partnerLimits;

        public PartnerBuilder WithId(Guid id)
        {
            _id = id;
            return this;
        }

        public PartnerBuilder WithName(string name)
        {
            _name = name;
            return this;
        }

        public PartnerBuilder WithIsActive(bool isActive)
        {
            _isActive = isActive;
            return this;
        }

        public PartnerBuilder WithPartnerLimits(ICollection<PartnerPromoCodeLimit> partnerLimits)
        {
            _partnerLimits = partnerLimits;
            return this;
        }

        public Partner Build()
        {
            return new Partner()
            {
                Id = _id,
                Name = _name,
                IsActive = _isActive,
                PartnerLimits = _partnerLimits
            };
        }
    }
}
