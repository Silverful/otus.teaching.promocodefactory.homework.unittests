﻿using Otus.Teaching.PromoCodeFactory.Core.Domain.PromoCodeManagement;
using System;

namespace Otus.Teaching.PromoCodeFactory.UnitTests.Builders
{
    public class PartnerPromoCodeLimitBuilder
    {
        private Guid _id;
        private DateTime _createDate;
        private DateTime _endDate;
        private int _limit;

        public PartnerPromoCodeLimitBuilder WithId(Guid id)
        {
            _id = id;
            return this;
        }

        public PartnerPromoCodeLimitBuilder WithCreateDate(DateTime createDate)
        {
            _createDate = createDate;
            return this;
        }

        public PartnerPromoCodeLimitBuilder WithEndDate(DateTime endDate)
        {
            _endDate = endDate;
            return this;
        }

        public PartnerPromoCodeLimitBuilder WithLimit(int limit)
        {
            _limit = limit;
            return this;
        }

        public PartnerPromoCodeLimit Build()
        {
            return new PartnerPromoCodeLimit()
            {
                Id = _id,
                CreateDate = _createDate,
                EndDate = _endDate,
                Limit = _limit
            };
        }
    }
}
